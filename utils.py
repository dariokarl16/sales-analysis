import os
import errno


def download_and_unzip(dataset_name, datapath):
    import kaggle
    kaggle.api.authenticate()
    kaggle.api.dataset_download_files(dataset_name, path=datapath, unzip=True)


def unzip(filepath):
    if os.path.isfile(filepath):
        from zipfile import ZipFile
        with ZipFile(filepath, 'r') as f:
            f.extractall(os.path.dirname(filepath))
    else:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filepath)


def load_and_analyze_data(df):
    '''
    Takes a dataframe and applies several 'inspecting' methods from pandas.
    The goal is to check whether there are any nulls in data, value datatypes, 
    '''
    print(df.sample(10))
    print(df.info())
    print(df.describe())
    print(df.isna().sum())


